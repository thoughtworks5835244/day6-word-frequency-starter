import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordFrequencyGame {

    private static final String SEPARATOR = "\\s+";

    public String getResult(String sentence) {
        try {
            var words = sentence.split(SEPARATOR);
            var counts = countWords(words);
            return formatResultString(counts);

        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private List<WordCount> countWords(String[] words) {
        var counts = new ArrayList<WordCount>();

        Stream.of(words).forEach(word ->
            counts.stream()
                .filter(w -> w.getWord().equals(word))
                .findFirst()
                .ifPresentOrElse(
                    WordCount::increment,
                    () -> counts.add(new WordCount(word, 1))
                )
        );

        return counts;
    }

    private String formatResultString(List<WordCount> counts) {
        return counts.stream()
            .sorted(Comparator.comparing(WordCount::getCount).reversed())
            .map(w -> w.getWord() + " " + w.getCount())
            .collect(Collectors.joining("\n"));
    }
}
